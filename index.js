// express package was imported as express
const express = require("express");

// invoked packaged to create a server/api and saved it in variable which we can refer later to create routes
const app = express();

// express.json() is a method from express that allow us to handle the stream of data from our client and receive the dat and automatically parse the incoming JSON from request

// app.us() is a method used to run another function or method from our expressjs api


// variable for port assignment
const port = 4000;


// Mock collection of Courses
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
]

let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell",
	},
	{
		email: "janedoePriest",
		password: "merrymarybell",
	},
	{
		email: "kimTofu",
		password: "dubuTofu",
	}
	
]

// used the listen() method of express to assign a port to our server and send a message

// Creating route in Express:
// access express to have access to its route methods
/*
Syntax:
		app.method('/endpoint',(request,response)=>{

		// send is a method similar to end() that it send the data/message and ends the response.

		//it also automatically creates and adds the headers.
		response.send
		})
*/

// it is used to run middleware(functions that add features to our application)
app.use(express.json());

app.get('/',(req,res)=>{

	res.send("hello from our first ExpressJS route!");

})

app.post('/',(req,res)=>{
	res.send("Hellow from our first ExpressJS Post Method Route");
})

app.put('/',(req,res)=>{
	res.send("Hello from a PUT method route!");
})

app.delete('/',(req,res)=>{
	res.send("Hello from a DELETE method route!");
})

app.get('/courses',(req,res)=>{
	res.send(courses);
})

// Create a route to be able to add a new course from an input from a request
app.post('/courses',(req,res)=>{

	console.log(req.body);
	courses.push(req.body);
	console.log(courses);
	res.send(courses);
})

app.get('/users',(req,res)=>{
	res.send(users);
})

app.post('/users',(req,res)=>{
	console.log(req.body);
	users.push(req.body);
	console.log(users);
	res.send(users);

})

app.delete('/users',(req,res)=>{
	users.pop();
	res.send(users);
})

app.patch('/courses',(req,res)=>{
	courses[req.body.path].price = req.body.value;
	res.send(courses);
})

app.listen(port,() => console.log(`Express API running at Port 4000`))